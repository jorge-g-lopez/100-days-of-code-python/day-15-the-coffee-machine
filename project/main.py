"""The Coffee Machine Project"""

MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    },
}

UNITS = {
    "water": "ml",
    "milk": "ml",
    "coffee": "g",
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}

money = 0
on = True


def check_resources(coffee):
    """Return True if there are enough resources for making the coffee"""
    for ingredient in MENU[coffee]["ingredients"]:
        if resources[ingredient] < MENU[coffee]["ingredients"][ingredient]:
            print(f"Sorry there is not enough {ingredient}.")
            return False
    return True


def get_money(coffee):
    """Return True if enough money for the coffee was inserted"""
    print("Please insert coins.")
    inserted_money = int(input("How many quarters?: ")) * 0.25
    inserted_money += int(input("How many dimes?: ")) * 0.1
    inserted_money += int(input("How many nickels?: ")) * 0.05
    inserted_money += int(input("How many pennies?: ")) * 0.01

    change = round(inserted_money - MENU[coffee]["cost"], 2)

    if change < 0:
        print("Sorry that's not enough money. Money refunded.")
        return False
    if change > 0:
        print(f"Here is ${change} dollars in change.")
    return True


def deliver_coffee(coffee):
    """Delivers the coffee requested, substracts the ingredients
    used from the inventory and adds the sale to the money total"""
    global money, resources

    money += MENU[coffee]["cost"]
    for ingredient in MENU[coffee]["ingredients"]:
        resources[ingredient] -= MENU[coffee]["ingredients"][ingredient]
    print(f"Here is your {coffee}. Enjoy!")


while on:
    order = input("What would you like? (espresso/latte/cappuccino): ").lower()

    if order == "report":
        for resource in resources:
            inv = resource.title() + ": " + str(resources[resource]) + UNITS[resource]
            print(inv)
        print(f"Money: ${money}")
    elif order == "off":
        on = False
    elif order in MENU:
        if check_resources(order):
            if get_money(order):
                deliver_coffee(order)
    else:
        print("Wrong selection, please try again.")
